import java.io.BufferedReader;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Normalizer;

public class Normalizar {
    static boolean first;

    public static String normalize(char[] buffer) {
        String s = Normalizer.normalize(new String(buffer), Normalizer.Form.NFD);
        s = s.toLowerCase();
        s = s.replaceAll("[\\w+\\.]?\\w+\\.\\w+[/\\w+\\.\\w]*", ""); // quitar urls
        s = s.replaceAll("\n", " ");        // quitar saltos de línea
        s = s.replaceAll("\\[\\S+\\]", ""); // quitar citas esilo [fn#08]
        s = s.replaceAll("[^a-z0-9 ']", ""); // quitar no (alfanuméricos, espacios, apóstrofe)
        s = s.replaceAll(" +"," ");         // quitar espacios duplicados
        if (first) {
            s = s.replaceAll("^ ", "");     // si es la primera línea, quitar el espacio inicial
            first = false;
        }
        return s;
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.printf("Uso: [Normalizer] [archivo entrada]\n");
            return;
        }
        String filename = args[0];
        String outname = filename+".cln";

        BufferedReader reader = null;
        BufferedWriter writer = null;
        String normalizedText;
        first = true;
        try {
            reader = new BufferedReader(new FileReader(filename));
            writer = new BufferedWriter(new FileWriter(outname));
            int BUFSIZ = 1024;
            int rd = -1;
            //int count = 0;
            char[] buffer = new char[BUFSIZ];
            while ((rd = reader.read(buffer, 0, BUFSIZ)) > -1) {
                normalizedText = normalize(buffer);
                //System.out.println("<"+normalizedText+">");
                writer.write(normalizedText);
                /*
                if (++count>3) {
                    return;
                }
                */
            }

        } catch (Exception e) {
            System.err.format("Excepción intentando leer '%s'.", filename);
            e.printStackTrace();
        } finally {
            try{
                if (reader != null) {
                    reader.close();
                }
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
