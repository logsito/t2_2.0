import java.io.Serializable;
import java.util.HashMap;

public class Node implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3858792847161241136L;
	int start, end = SuffixTree.oo, link,largo,numofleaves;
    public HashMap<Character, Integer> next = new HashMap<Character, Integer>();

    public Node(int start, int end) {
        this.start = start;
        this.end = end;
        this.numofleaves=0;
        this.largo= start - end;
    }
    public void setNumLeaves(int num) {
    	this.numofleaves=num;
    }
    
    public int getNumLeaves() {
    	return this.numofleaves;
    }

    public int edgeLength(SuffixTree st) {
        return Math.min(end, st.position + 1) - start;
    }

    public boolean hasChildren() {
        return next.size() != 0;
    }
}
