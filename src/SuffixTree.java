import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Collections;
import java.util.Comparator;

public class SuffixTree implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -813831372353609382L;
	public static final int oo = Integer.MAX_VALUE/2;
    public Node [] nodes;
    public char [] text;
    public int root, position = -1,
            	currentNode,
            	needSuffixLink,
            	remainder, numofleaves;

    public int active_node, active_length, active_edge;

    public SuffixTree(int length) {
        nodes = new Node[2* length + 2];
        text = new char[length];
        root = active_node = newNode(-1, -1);
    }

    public SuffixTree(String text) {
        this(text.length());
        for (int i = 0; i < text.length(); i++) {
            try {
                this.addChar(text.charAt(i));
            } catch (Exception e) {
            }
        this.fillnumofchildren(this.root);
        }
    }
    
        
    
    
       public void topkq(int k, int q,int x, int acumulado, ArrayList<Node> Nodos ) { 
    	 for (int child : this.nodes[x].next.values()) {
    		 //System.out.print(this.nodes[child]);
    		 //System.out.print('\n');
    		 //System.out.print(this.nodes[x].largo + acumulado);
    		 int largo= this.nodes[x].largo + acumulado;
    		 //System.out.print(q);
    		/*si soy menoslargo de lo que busco sigo buscando :( */
    	    if (largo < q) {
    	    	//System.out.print("SOY PQe");
        		//System.out.print('\n');
    			acumulado+= this.nodes[child].end-this.nodes[child].start;
    			topkq(k,q,child,acumulado,Nodos);
    	   }
    	/*si soy del mismo largo de lo que busco no sigo y me guardo */
    	   else if (largo == q ){
    		    //System.out.print("ESTOY READY");
    		   	//System.out.print(acumulado);
    		   	Nodos.add(this.nodes[child]);
    	   }
    		/*si soy más largo de lo que busco no sigo y descarto a mis hijos */
    	   //System.out.print("HELP SOY GRANDE");
    	   //System.out.print('\n');
    	}
    	 
    }
    
    public ArrayList<Node> topKQ(int k, int q){
    	ArrayList<Node> Nodos = new ArrayList<Node>();
    	ArrayList<Node> Devolver = new ArrayList<Node>();
    	topkq(k,q,this.root,0,Nodos);
    	int i = 0 ;
    	while(i<k && !Nodos.isEmpty()) {
    		Devolver.add(Collections.max(Nodos, new NodeComparator()));
    		i++;
    	}
    	return Devolver;
    }
    
    public int fillnumofchildren(int x) {
    	if (this.nodes[x].next.size() == 0) {
    		this.nodes[x].setNumLeaves(1);
    		return 1;
    	}
    	else if (x != this.root) {
    		for (int child :this.nodes[x].next.values()) {
    			this.nodes[x].numofleaves +=fillnumofchildren(child);
    			
    		}
    		return this.nodes[x].numofleaves;
    		
    	}
        else {
            for (int child : this.nodes[x].next.values()) {
                this.numofleaves =this.numofleaves + this.fillnumofchildren(child);
                }
            
            }
    	return this.numofleaves;    	
            
    }

    public void addSuffixLink(int node) {
        if (needSuffixLink > 0)
            nodes[needSuffixLink].link = node;
        needSuffixLink = node;
    }

    public char active_edge() {
        return text[active_edge];
    }

    public boolean walkDown(int next) {
        if (active_length >= nodes[next].edgeLength(this)) {
            active_edge += nodes[next].edgeLength(this);
            active_length -= nodes[next].edgeLength(this);
            active_node = next;
            return true;
        }
        return false;
    }

    public int newNode(int start, int end) {
        nodes[++currentNode] = new Node(start, end);
        return currentNode;
    }

    public void addChar(char c) throws Exception {
        text[++position] = c;
        needSuffixLink = -1;
        remainder++;
        while(remainder > 0) {
            if (active_length == 0) active_edge = position;
            if (!nodes[active_node].next.containsKey(active_edge())){
                int leaf = newNode(position, oo);
                nodes[active_node].next.put(active_edge(), leaf);
                addSuffixLink(active_node); //rule 2
            } else {
                int next = nodes[active_node].next.get(active_edge());
                if (walkDown(next)) continue;   //observation 2
                if (text[nodes[next].start + active_length] == c) { //observation 1
                    active_length++;
                    addSuffixLink(active_node); // observation 3
                    break;
                }
                int split = newNode(nodes[next].start, nodes[next].start + active_length);
                nodes[active_node].next.put(active_edge(), split);
                int leaf = newNode(position, oo);
                nodes[split].next.put(c, leaf);
                nodes[next].start += active_length;
                nodes[split].next.put(text[nodes[next].start], next);
                addSuffixLink(split); //rule 2
            }
            remainder--;

            if (active_node == root && active_length > 0) {  //rule 1
                active_length--;
                active_edge = position - remainder + 1;
            } else
                active_node = nodes[active_node].link > 0 ? nodes[active_node].link : root; //rule 3
        }
    }

    String edgeString(int node) {
    	return new String(Arrays.copyOfRange(text, nodes[node].start, Math.min(position + 1, nodes[node].end)));
        //return pathString(node, node);
    }

    String pathString(int nodeAncestor, int nodeDescendant) {
        String substr = new String(Arrays.copyOfRange(text, nodes[nodeAncestor].start, Math.min(position + 1, nodes[nodeDescendant].end)));
        //System.out.printf("pathString(%d, %d): '%s'\n", nodeAncestor, nodeDescendant, substr);
        return substr;
    }

    void printTree() {
        Queue<Pair<Integer,Integer>> descendants = new ConcurrentLinkedQueue<Pair<Integer,Integer>>();
        descendants.add(new Pair<Integer,Integer>(root, 0));

        Pair nodeDepth;
        while ((nodeDepth = descendants.poll()) != null) {
            int node = (Integer) nodeDepth.getLeft();
            int depth = (Integer) nodeDepth.getRight();
            printNode(node, depth);

            Node n = this.nodes[node];
            for (int child : n.next.values()) {
                descendants.add(new Pair<Integer,Integer>(child, depth+1));
            }
        }
    }

    void printNode(int node, int depth) {
        Node n = this.nodes[node];
        String tab = String.join("", Collections.nCopies(depth, "\t"));
        //System.out.printf("%sNodo %d: edgeString: '%s'", tab, node, node == root ? "(raíz)" : edgeString(node));
        //System.out.printf(" start: %d, end: %d", n.start, n.end);
        if (this.nodes[node].hasChildren()) {
            //System.out.printf(", hijos: %s\n", new ArrayList<Integer>(n.next.values()).toString()    );
        } else {
            //System.out.printf("\n");
        }
    }

    // locate
    public List<Integer> locate(String needle) {
        return locate(this.root, needle);
    }

    List<Integer> locate(int node, String suffix) {
        return locate(node, suffix, 0);
    }

    List<Integer> locate(int node, String suffix, int offset) {
        //System.out.printf("llamado locate(%d, %s, %d)\n", node, suffix, offset);
        List<Integer> posiciones = new ArrayList<Integer>();
        String effectiveSuffix = suffix.substring(offset); // parte del string que estoy buscando en esta llamada
        //System.out.printf("needle: '%s'\n", suffix);
        //System.out.printf("needle efectivo: '%s'\n", effectiveSuffix);

        for (int child : this.nodes[node].next.values()) {
            String edgeLabel = edgeString(child);
            //System.out.printf("\t- nodo %d: '%s'\n", child, edgeLabel);
            if (edgeLabel.startsWith(effectiveSuffix)) {
                posiciones.addAll(locateIndices(child, offset));
            } else if (effectiveSuffix.startsWith(edgeLabel)) {
                posiciones.addAll(locate(child, suffix, edgeLabel.length() + offset));
            }
        }
        return posiciones;
    }

    List<Integer> locateIndices(int node, int prefixLength) {
        /* Dado el nodo de índice node, busca todas sus hojas, consigue
         * la posición start de cada nodo hoja, y le resta el largo del
         * substring entre el nodo y la hoja */
        //System.out.printf("llamado locateIndices(%d, %d)\n", node, prefixLength);

        List<Integer> posiciones = new ArrayList<Integer>();

        /* descendants almacena una lista de tuplas; el primer elemento
         * corresponde al índice del nodo, el segundo corresponde al
         * largo del substring, desde el nodo acumulado, hasta cada
         * iteración. Cuando el nodo sea una hoja, se resta el largo del
         * substring */
        //System.out.printf("\tetiqueta del arco: '%s'\n", edgeString(node));
        if (this.nodes[node].hasChildren()) {
            Queue<Pair> descendants = new ConcurrentLinkedQueue<Pair>();
            descendants.add(new Pair<Integer, Integer>(node, 0));

            Pair nodeLength;
            while ((nodeLength = descendants.poll()) != null) {
                int indexNode = (Integer) nodeLength.getLeft();
                int cumulativeLength = (Integer) nodeLength.getRight();
                //System.out.printf("\tnodo desc: %d\n", indexNode);
                for (int child : this.nodes[indexNode].next.values()) {
                    Node descendant = this.nodes[child];
                    //System.out.printf("\tnodo hijo: %d\n", child);
                    if (edgeString(child).equals("$")) { // Caso Maldito
                        int dollarPos = this.nodes[indexNode].start - prefixLength;
                        //System.out.printf("hola soy el hijo %d (un peso), cumlen %d, "+
                         //                 "voy a hacer que el nodo %d agregue la posición %d\n", child, cumulativeLength, indexNode, dollarPos);
                        posiciones.add(dollarPos);
                        continue;
                    }
                    if (descendant.hasChildren()) { // no es hoja
                        int edgeLength = edgeString(child).length();
                        descendants.add(new Pair<Integer, Integer>(child, cumulativeLength + edgeLength));
                    } else { // es hoja
                        Node leaf = descendant;
                        int startOccurrence = leaf.start - cumulativeLength;
                        int pos = startOccurrence - prefixLength;
                        //System.out.printf("\t%d es hoja (%s): start %d, prefixlength: %d, cumlength: %d, posición: %d\n", child, edgeString(child), leaf.start, prefixLength, cumulativeLength, pos);
                        posiciones.add(pos);
                    }
                }
            }
        } else {
            printNode(node, 1);
            posiciones.add(this.nodes[node].start - prefixLength);
        }
        return posiciones;
    }

    // count
    public int count(String needle) {
        return locate(needle).size();
    }

    public void writeSt(String s) {
		try {
			FileOutputStream f = new FileOutputStream(new File(s));
			ObjectOutputStream o = new ObjectOutputStream(f);

			o.writeObject(this);

			o.close();
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println("Archivo no encontrado");
		} catch (IOException e) {
			System.out.println("Error abriendo archivo");
		}
	}
    
    public static SuffixTree readSt(String s) {
		try {
			FileInputStream fi = new FileInputStream(new File(s));
			ObjectInputStream oi = new ObjectInputStream(fi);

			SuffixTree st = (SuffixTree) oi.readObject();

			oi.close();
			fi.close();

			return st;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
			System.out.println("Archivo no encontrado");
		} catch (IOException e) {
			System.out.println("Error abriendo archivo");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		// No debería llegar a este null
		return null;
	}


    public static void main(String[] args) {
        String texto, needle;
        if (args.length > 1) {
            texto = args[0];
            needle = args[1];
        } else {
            texto = "pantmantjantx$";
            needle = "ant";
        }

        System.out.printf("Texto: '%s'\t- Buscando: '%s'\n", texto, needle);
        SuffixTree st = new SuffixTree(texto);

        st.printTree();
        System.out.println("------------");
        List<Integer> pos = st.locate(needle);

        System.out.println(pos);

    }

}

class Pair<L,R> {

  private final L left;
  private final R right;

  Pair(L left, R right) {
    this.left = left;
    this.right = right;
  }

  L getLeft() { return left; }
  R getRight() { return right; }

  @Override
  public int hashCode() { return left.hashCode() ^ right.hashCode(); }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Pair)) return false;
    Pair pairo = (Pair) o;
    return this.left.equals(pairo.getLeft()) &&
           this.right.equals(pairo.getRight());
  }

}

class NodeComparator implements Comparator<Node>{ 
    
    // Overriding compare()method of Comparator  
     @Override
	 public int compare(Node a , Node b ) {
 		if (a.getNumLeaves() > b.getNumLeaves())
 			return -1; // highest value first
 		else if (a.getNumLeaves() < b.getNumLeaves())
 			return 1;
 		else {
 			return 0 ; 
 		}
 }
}