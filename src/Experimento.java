import java.io.BufferedReader;
import java.io.FileReader;

public class Experimento {
	public static long insert(String file, String save) throws Exception{

        BufferedReader reader = null;
        StringBuilder bob = new StringBuilder();
        String texto = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            int BUFSIZ = 1024;
            int rd = -1;
            char[] buffer = new char[BUFSIZ];
            while ((rd = reader.read(buffer, 0, BUFSIZ)) > -1) {
                bob.append(buffer);
            }
            bob.append("$");
            texto = bob.toString().replace("\0", "").replace("\n","");


        } catch (Exception e) {
            System.err.format("Excepción intentando leer '%s'.", file);
            e.printStackTrace();
        }

        SuffixTree st = new SuffixTree(texto.length());

		long start = System.currentTimeMillis();
		for(int i =0; i<texto.length();i++) {
			st.addChar(texto.charAt(i));
		}
		long end = System.currentTimeMillis();
		
		if(save.equals("s")) {
			System.out.println("Árbol construido. Guardando en disco...");
			st.writeSt(file.split("_")[0] + "_st");	
		}
		//ST printer = new ST(st);
		
		
		
		return end - start;
	}

	
	public static long buscar(String file, String save) throws Exception {
		BufferedReader reader = null;
        StringBuilder bob = new StringBuilder();
        String texto = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            int BUFSIZ = 1024;
            int rd = -1;
            char[] buffer = new char[BUFSIZ];
            while ((rd = reader.read(buffer, 0, BUFSIZ)) > -1) {
                bob.append(buffer);
            }
            bob.append("$");
            texto = bob.toString().replace("\0", "").replace("\n","");


        } catch (Exception e) {
            System.err.format("Excepción intentando leer '%s'.", file);
            e.printStackTrace();
        }

        SuffixTree st = new SuffixTree(texto.length());

		
		for(int i =0; i<texto.length();i++) {
			st.addChar(texto.charAt(i));
		}
		
		if(save.equals("s")) {
			System.out.println("Árbol construido. Guardando en disco...");
			st.writeSt(file.split("_")[0] + "_st");	
		}
		//ST printer = new ST(st);
		BufferedReader newreader = new BufferedReader(new FileReader(file+"_words"));
		long start = System.currentTimeMillis();
		String line;
		while((line = newreader.readLine()) != null) {
			st.locate(line);
		}

		long end = System.currentTimeMillis();
		
		return end - start;
	}
	
	public static long contar(String file, String save) throws Exception {
		BufferedReader reader = null;
        StringBuilder bob = new StringBuilder();
        String texto = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            int BUFSIZ = 1024;
            int rd = -1;
            char[] buffer = new char[BUFSIZ];
            while ((rd = reader.read(buffer, 0, BUFSIZ)) > -1) {
                bob.append(buffer);
            }
            bob.append("$");
            texto = bob.toString().replace("\0", "").replace("\n","");


        } catch (Exception e) {
            System.err.format("Excepción intentando leer '%s'.", file);
            e.printStackTrace();
        }

        SuffixTree st = new SuffixTree(texto.length());

		
		for(int i =0; i<texto.length();i++) {
			st.addChar(texto.charAt(i));
		}
		
		if(save.equals("s")) {
			System.out.println("Árbol construido. Guardando en disco...");
			st.writeSt(file.split("_")[0] + "_st");	
		}
		//ST printer = new ST(st);
		BufferedReader newreader = new BufferedReader(new FileReader(file+"_words"));
		long start = System.currentTimeMillis();
		String line;
		while((line = newreader.readLine()) != null) {
			st.count(line);
		}

		long end = System.currentTimeMillis();
		
		return end - start;
	}
	
	
	public static void main(String[] args) throws Exception {
		//long t = insert(args[0], args[1]);
		//System.out.println("crear;"+args[0].split("_")[1]+";"+Long.toString(t));
		
		//long t = buscar(args[0],args[1]);
		//System.out.println("locate;"+args[0].split("_")[1]+";"+Long.toString(t));
		
		long t = contar(args[0],args[1]);
		System.out.println("contar;"+args[0].split("_")[1]+";"+Long.toString(t));
		
		
		
		//buscar("test_sw");
	}

}
