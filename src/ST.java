import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ST {
	 BufferedReader in;
	 PrintWriter out;

	 public ST() throws Exception {
        in = new BufferedReader(new InputStreamReader(System.in));
        out = new PrintWriter(new FileWriter("st.dot"));
        String line = in.readLine();
        SuffixTree st = new SuffixTree(line.length());
        for (int i = 0; i < line.length(); i++)
            st.addChar(line.charAt(i));
        printTree(st);
        out.close();
}
	 
	 public ST(SuffixTree st) throws Exception {
		 out = new PrintWriter(new FileWriter("testw.dot"));
		 printTree(st);
		 out.close();
	 }

	void printTree(SuffixTree st) {
        out.println("digraph {");
        out.println("\trankdir = LR;");
        out.println("\tedge [arrowsize=0.4,fontsize=10]");
        out.println("\tnode1 [label=\"\",style=filled,fillcolor=lightgrey,shape=circle,width=.1,height=.1];");
        out.println("//------leaves------");
        printLeaves(st.root, st);
        out.println("//------internal nodes------");
        printInternalNodes(st.root, st);
        out.println("//------edges------");
        printEdges(st.root, st);
        out.println("//------suffix links------");
        printSLinks(st.root, st);
        out.println("}");
    }

    void printLeaves(int x, SuffixTree st) {
        if (st.nodes[x].next.size() == 0)
            out.println("\tnode"+x+" [label=\"\",shape=point]");
        else {
            for (int child : st.nodes[x].next.values())
                printLeaves(child, st);
        }
    }

    void printInternalNodes(int x, SuffixTree st) {
        if (x != st.root && st.nodes[x].next.size() > 0)
            out.println("\tnode"+x+" [label=\"\",style=filled,fillcolor=lightgrey,shape=circle,width=.07,height=.07]");

        for (int child : st.nodes[x].next.values())
            printInternalNodes(child, st);
    }

    void printEdges(int x, SuffixTree st) {
        for (int child : st.nodes[x].next.values()) {
            out.println("\tnode"+x+" -> node"+child+" [label=\""+st.edgeString(child)+"\",weight=3]");
            printEdges(child, st);
        }
    }

    void printSLinks(int x, SuffixTree st) {
        if (st.nodes[x].link > 0)
            out.println("\tnode"+x+" -> node"+st.nodes[x].link+" [label=\"\",weight=1,style=dotted]");
        for (int child : st.nodes[x].next.values())
            printSLinks(child, st);
    }

    public static void main(String ... args) throws Exception{
        new ST();
    }
}
