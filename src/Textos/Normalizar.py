import re
import random
from math import ceil

def clean_and_split():
    filename1="dna"
    filename2="english" 

    file1= open(filename1,'r')
    file2= open(filename2,encoding='cp1252')

    DNA= file1.read()
    ENGLISH = file2.read()


    #Limpiar la cosa

    DNA= DNA.strip('\n')
    DNA=' '.join(DNA.split())
    DNA=DNA.lower()
    regex= re.compile('[^a-z0-9 ]')
    DNA=regex.sub('',DNA)

    ENGLISH= ENGLISH.strip('\n')
    ENGLISH=' '.join(ENGLISH.split())
    ENGLISH=ENGLISH.lower()
    regex= re.compile('[^a-z0-9 ]')
    ENGLISH=regex.sub('',ENGLISH)

    #Guardar los ejemplos
    for i in range(0,14):
        print(i)
        outputfile1= open("DNA_"+str(i), 'w')
        outputfile2= open("ENGLISH_"+str(i), 'w')
        outputfile1.write(DNA[0:2**(10+i)])
        outputfile2.write(ENGLISH[0:2**(10+i)])
        outputfile1.close()
        outputfile2.close()

def save_words(ex):
    filename1="dna"
    filename2="english" 

    file1= open(filename1,'r')
    file2= open(filename2,encoding='cp1252')

    DNA= file1.read()
    ENGLISH = file2.read()

    DNA= DNA.strip('\n')
    DNA=' '.join(DNA.split())
    DNA=DNA.lower()
    regex= re.compile('[^a-z0-9 ]')
    DNA=regex.sub('',DNA)

    ENGLISH= ENGLISH.strip('\n')
    ENGLISH=' '.join(ENGLISH.split())
    ENGLISH=ENGLISH.lower()
    regex= re.compile('[^a-z0-9 ]')
    ENGLISH=regex.sub('',ENGLISH)
    
    #Guardar las palabras
    outputfile1= open("DNA_{}_words".format(ex), 'w')
    outputfile2= open("ENGLISH_{}_words".format(ex), 'w')
    for i in range(0,int(2**(10+ex)/10)+1):
        #print(i)
        inicio=int(random.random()*(2**23))
        posibles=[8,16,12,64]
        candidato = DNA[inicio:inicio + posibles[int(random.random()*4)] ]
        if(random.random()<0.2):
            c = list(candidato)
            random.shuffle(c)
            candidato=''.join(c)
            print(candidato)
        outputfile1.write(candidato+'\n')
    ENGLISH=ENGLISH.split()
    for i in range(0,int(2**(10+ex)/10)):
        candidato = ENGLISH[int(random.random()*len(ENGLISH))]
        if(random.random()<0.2):
            c = list(candidato)
            random.shuffle(c)
            candidato=''.join(c)
            print(candidato)
        outputfile2.write(candidato+'\n')
    outputfile1.close()
    outputfile2.close()
    file1.close()
    file2.close()

    

if __name__ == '__main__':
    for i in range(14):
        save_words(i)
